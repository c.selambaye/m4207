#include <LWiFi.h>
#include <LWiFiUdp.h>

char ssid[] = "AndroidAP";  //  your network SSID (name)
char pass[] = "ttkk0914";       // your network password

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);

  Serial.println("setup()");

  // attempt to connect to Wifi network:
  LWiFi.begin();
  while (!LWiFi.connectWPA(ssid, pass))
  {
    delay(1000);
    Serial.println("retry WiFi AP");
  }
  Serial.println("Connected to wifi");
  printWifiStatus();

  delay(10000);

  Serial.println("setup() done");
}

void loop(){}

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // print your LWiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}










